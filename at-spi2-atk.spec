Name:          at-spi2-atk
Version:       2.38.0
Release:       4
Summary:       GTK+ module for the Assistive Technology Service

License:       LGPLv2+
URL:           https://www.freedesktop.org/wiki/Accessibility/AT-SPI2/
Source0:       https://github.com/GNOME/at-spi2-atk/archive/AT_SPI2_ATK_2_38_0.tar.gz

Patch6000:     backport-fix-test-memory-leak.patch
Patch6001:     backport-also-fix-ref-leak-in-try_get_root.patch

BuildRequires: gtk2-devel dbus-devel dbus-glib-devel gcc
BuildRequires: libxml2-devel gettext glib2-devel meson
BuildRequires: atk-devel >= 2.29.2 dbus-x11
BuildRequires: at-spi2-core-devel >= 2.33.2
BuildRequires: gsettings-desktop-schemas

Requires:      atk >= 2.33.3
Requires:      at-spi2-core >= 2.33.2

%description
At-Spi2 is a protocol over DBus, toolkit widgets use it to provide
their content to screen readers such as Orca.

The server side, implemented in atk and at-spi2-atk, is basically
the server implementation for all the RPC calls that one would make
with the client-side library.

%package devel
Summary:       The header files for %{name}
Requires:      %{name} = %{version}-%{release}

%description devel
%{name}-devel package bridges ATK to D-Bus at-spi

%package_help

%prep
%autosetup -n at-spi2-atk-AT_SPI2_ATK_2_38_0 -p1

%build
%meson
%meson_build

%install
%meson_install

%check
dbus-launch ninja -C %{_host} test

%files
%doc AUTHORS
%license COPYING
%dir %{_libdir}/gtk-2.0
%{_libdir}/libatk-bridge-2.0.so.*
%{_libdir}/gnome-settings-daemon-3.0/*
%{_libdir}/gtk-2.0/modules/libatk-bridge.so

%files devel
%{_libdir}/pkgconfig/atk-bridge-2.0.pc
%{_libdir}/libatk-bridge-2.0.so
%{_includedir}/at-spi2-atk/2.0/atk-bridge.h

%files help
%doc README

%changelog
* Tue Apr 23 2024 lingsheng <lingsheng1@h-partners.com> - 2.38.0-4
- add gsettings-desktop-schemas to fix check fail

* Fri Sep 15 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 2.38.0-3
- enable make check

* Sat Oct 22 2022 wangkerong <wangkerong@h-partners.com> - 2.38.0-2
- fix memory leak in test

* Tue Jan 28 2020 yanglu <yanglu60@huawei.com> - 2.38.0-1
- Version update

* Mon Jul 20 2020 wangye <wangye70@huawei.com> - 2.34.2-1
- Version update

* Tue Sep 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.30.0-2
- Package Init
